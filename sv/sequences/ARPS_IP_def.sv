/****************************************************************************
    +-+-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+-+-+-+-+ +-+-+ +-+-+-+-+-+-+-+-+
    |F|u|n|c|t|i|o|n|a|l| |V|e|r|i|f|i|c|a|t|i|o|n| |o|f| |H|a|r|d|w|a|r|e|
    +-+-+-+-+-+-+-+-+-+-+ +-+-+-+-+-+-+-+-+-+-+-+-+ +-+-+ +-+-+-+-+-+-+-+-+

    FILE            ARPS_IP_master_base_seq.sv

    DESCRIPTION     base sequence to be extended by other sequences

 ****************************************************************************/

`ifndef ARPS_IP_DEF_SV
`define ARPS_IP_DEF_SV

class ARPS_IP_def;
    
    //FOR TEST 1 (txt files)
    int ref_frame_num = 50; //starting txt file in folder "images_for_arps" (sample50.txt)
    int curr_frame_num = 51;
    
    int num_of_seq = 6;// for TEST 2 (4 corner case + N random case)
    
    //FOR TEST 2 (4 corner cases and random)
    int start_frame_c = 4;// start from seq 4 (if 4 don't include 4 corner case, if val is 0 include 4 corener case)  
    int start_frame_r = 4; 
    
    function int get_num_of_seq();
        return num_of_seq;
    endfunction
    
    function int get_ref_frame_num();
        return ref_frame_num;
    endfunction
    
    function int get_curr_frame_num();
        return curr_frame_num;
    endfunction
    
    function int get_start_frame_c();
        return start_frame_c;
    endfunction
    
    function int get_start_frame_r();
        return start_frame_r;
    endfunction
    
endclass

`endif